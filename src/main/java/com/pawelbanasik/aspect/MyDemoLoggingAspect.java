package com.pawelbanasik.aspect;

import java.util.List;
import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.pawelbanasik.domain.Account;

@Aspect
@Component
@Order(2)
public class MyDemoLoggingAspect {

	private Logger myLogger = Logger.getLogger(getClass().getName());
	
	@Around("execution(* com.pawelbanasik.serviceimpl.TrafficFortuneService.getFortune(..))")
	public Object aroundGetFortune(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		
		String methodSignature = proceedingJoinPoint.getSignature().toShortString();
		myLogger.info("\n=====> Executing @Around finally: " + methodSignature);
		
		long begin = System.currentTimeMillis();
		
		Object result = proceedingJoinPoint.proceed();
		
		long end = System.currentTimeMillis();
		
		long duration = end - begin;
		
		myLogger.info("\n====> Duration: " + duration / 1000.0 + " seconds.");
		
		return result;
	}
	
	
	@After("execution(* com.pawelbanasik.dao.AccountDao.findAccounts(..))")
	public void afterFinallyFindAccountAdvice(JoinPoint joinPoint) {
		String methodSignature = joinPoint.getSignature().toShortString();
		myLogger.info("\n=====> Executing @After finally: " + methodSignature);
	}
	
	@AfterThrowing(pointcut = "execution(* com.pawelbanasik.dao.AccountDao.findAccounts(..))", throwing = "theExc")
	public void afterThrowingFindAccountsAdvice(JoinPoint joinPoint, Throwable theExc) {
		String methodSignature = joinPoint.getSignature().toShortString();
		myLogger.info("\n=====> Executing @AfterThrowing on metod: " + methodSignature);
		myLogger.info("\n===> The exception is: " + theExc);
	}

	@AfterReturning(pointcut = "execution(* com.pawelbanasik.dao.AccountDao.findAccounts(..))", returning = "result")
	public void afterReturningFindAdvice(JoinPoint joinPoint, List<Account> result) {
		String methodSignature = joinPoint.getSignature().toShortString();
		myLogger.info("\n=====> Executing @AfterReturning on metod: " + methodSignature);
		myLogger.info("\n=====> result is: " + result);
		convertAccountNamesToUpperCase(result);
		myLogger.info("\n=====> result is: " + result);
	}

	private void convertAccountNamesToUpperCase(List<Account> result) {
		for (Account account : result) {
			String toUpperName = account.getName().toUpperCase();
			account.setName(toUpperName);
		}

	}

	@Before("com.pawelbanasik.aspect.LuvAopExpressions.forDaoPackageNoGetterSetter()")
	public void beforeAddAccountAdvice(JoinPoint joinPoint) {
		myLogger.info("\n=====> Executing @Before advice on addAccount()");
		MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
		myLogger.info("Method: " + methodSignature);
		Object[] args = joinPoint.getArgs();
		for (Object tempArg : args) {
			myLogger.info(tempArg.toString());
			if (tempArg instanceof Account) {
				Account account = (Account) tempArg;
				myLogger.info("account name: " + account.getName());
				myLogger.info("account level: " + account.getLevel());
			}

		}

	}
}
