package com.pawelbanasik.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.pawelbanasik.domain.Account;

@Component
public class AccountDao {

	private String name;
	private String serviceCode;

	public List<Account> findAccounts(boolean tripWire) {
		
		// simulation
		if (tripWire) {
			throw new RuntimeException("No soup for you");
		}
		
		List<Account> accounts = new ArrayList<Account>();
		Account temp1 = new Account("John", "Silver");
		Account temp2 = new Account("Madhu", "Platinum");
		Account temp3 = new Account("Luca", "Gold");
		accounts.add(temp1);
		accounts.add(temp2);
		accounts.add(temp3);
		return accounts;
	}

	public void addAccount(Account account, boolean vipFlag) {
		System.out.println(getClass().getSimpleName() + ": Doing my db work: Adding and account.");
	}

	public boolean doWork() {
		System.out.println(getClass().getSimpleName() + ": in doWork()");
		return false;
	}

	public String getName() {
		System.out.println(getClass().getSimpleName() + ": in getName()");
		return name;
	}

	public void setName(String name) {
		System.out.println(getClass().getSimpleName() + ": in setName()");
		this.name = name;
	}

	public String getServiceCode() {
		System.out.println(getClass().getSimpleName() + ": in getServiceCode()");
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		System.out.println(getClass().getSimpleName() + ": in setServiceCode()");
		this.serviceCode = serviceCode;
	}

}
